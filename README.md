#  **#Một số khái quát về Hosting**

	* Trình biên dịch Javascript sẽ chuyển phần khai báo của biến và hàm lên trên top, nó được gọi là hosting
	* Chỉ có phần khai báo được đưa lên top, không phải phần gán giá trị
	* Khai báo hàm được lên trước phần khai báo biến

# **#Sự khác nhau giữa var, let and const**

### **_var:_**
		* Có hosting, có tính chất globally scope
		 	VD: 1 biến được khai báo trong 1 hàm function nhưng khi thoát hàm function đó 
			 ra thì biến var ở ngoài giữ nguyên giá trị đã khai báo trong fucntion
	    * Được khởi tạo với giá trị undefined
	    * Dù khai báo ở đâu thì biến đều sẽ được đem lên đầu scope trước khi code được thực hiện. 

### **_let:_**
	    * có tính chất block scope (biến ra khỏi scope sẽ ko được sử dụng nữa)
	    * cho phép ta thay đổi giá trị biến, nhưng ko cho phép ta khai báo lại lần nữa
        * ko có bất cứ giá trị nào được khởi tạo

### **_const:_**
		* Có tính chất block scope (biến ra khỏi scope sẽ ko được sử dụng nữa)
		* Biến là primitive(string, int,..) ko thể khai báo hay cập nhật giá trị mới
		* Biến là unprimitive (object,array,fucntion) => có thể cập nhật được giá trị mới

# **#String**
	* str.include(searchString): kiểm tra chuỗi có trong string str ko? Đúng: True/Sai: False
		VD: "Hello World".include("World"); = > return true

	* str.substring(postion start, position end) cắt chuỗi
	* str1.concat(str2) / str1+str2 => nối chuỗi
	* str.startsWith(searchString)=> kiểm tra đầu chuỗi có là phần tử đó ko?
	  str.endsWith(searchString) => kiểm tra cuối chuối có là phần tử đó hay ko?
	* str.trim() loại bỏ space inline
	* convert string => sang hệ cơ số x
		VD: return Number.parsenInt(string,x) x: hệ cơ số 2,10,16,...

# **#Array**
	Là kiểu dữ liệu mà giá trị của nó chứa nhiều giá trị khác. Mỗi giá trị trong mảng được gọi là element.

### **_1. include()_**
	Method này giúp các bạn check sự tồn tại của item nào đó trong mảng. 
	Phương thức này trả về true nếu trong mảng chứa phần tử và false nếu không.
	
##### **Syntax**
	array.includes(element, start);

##### **Example**
	const arr = ['Duc', 1, 2, 3];
	arr.includes('Duc'); // output: true
	arr.includes(4); // output: false


### **_2. pop() & push()_**
	Method pop() trả về một array mới loại bỏ phần tử cuối cùng trong array cũ
	Method push() trả về một array mới thêm phần tử element vào cuối array mới
##### **Syntax**
	arr.pop();
	arr.push(element);
##### **Example**
	var fruits = ["Banana", "Orange", "Apple", "Mango"];
	fruits.pop();              // Removes the last element ("Mango") from fruits
	fruits.push("Kawaki");     // Add element ("Kawaki") to the last fruits

### **_3. toString() & join()_**
	toString() Method này convert an Array to a String (mỗi element value cách nhau bởi một dấu phẩy)
	join("mark") Method này convert an Array to a String (mỗi element value cách nhau bởi một ký kiệu "mark")
##### **Syntax**
	arr.toSring();
	arr.join();
##### **Example**	
	var fruits = ["Banana", "Orange", "Apple", "Mango"];
	console.log(fruits.toString()); //output: Banana,Orange,Apple,Mango
	console.log(fruits.join(*)); 	//output: Banana*Orange*Apple*Mango

### **_4. splice()**
	* Phương thức này có thể thêm các element vào trong array vị trí tùy ý 
	Và cũng có thể xóa các element trong array vị trí tùy ý
	* Tính deepcopy
##### **Syntax**
	arr.splice(position_add, numbers_element_remove, add_Element1, add_Element2,...)
##### **Example**
	var fruits = ["Banana", "Orange", "Apple", "Mango"];
	fruits.splice(2, 1, "Lemon", "Kiwi"); 	//output fruits = ["Banana", "Orange","Lemon", "Kiwi","Mango"];

### **_5. concat()_**
	* Phương thức nối các array
	* sallow copy
##### **Syntax**
	arr1.concat(arr2,arr3,arr4,..);
##### **Example**
	var arr1 = ["Cecilie", "Lone"];
	var arr2 = ["Emil", "Tobias", "Linus"];
	var arr3 = ["Robin", "Morgan"];
	var myChildren = arr1.concat(arr2, arr3);   // Concatenates arr1 with arr2 and arr3

### 6. _**slice()**_
	* Phương thức này cho phép các element từ array cũ sang một array mới
	* shallow copy
##### **Syntax**	
	arr.slice(position_start, position_end)
##### **Example**
	var fruits = ["Banana", "Orange", "Lemon", "Apple", "Mango"];
	var citrus = fruits.slice(1, 3); //output citrus = ["Orange", "Lemon"];

### _**7. sort()**_
	Phương thức này cho phép sắp xếp từ các element trong array
#### _**7a. String sort**_
	The sort() method sorts an array alphabetically string
##### **Syntax**
	arr.sort();
##### **Example**
	var fruits = ["Banana", "Orange", "Apple", "Mango"];
	fruits.sort();        // output: Apple,Banana,Mango,Orange


#### _**7b. Numberic sort()**_
##### **Syntax**
	arr.sort(function(a, b){return a - b}); sắp xếp bé đến lớn
	arr.sort(function(a, b){return b - a}); sắp xếp lớn đến bé
##### **Example**
	var points = [40, 100, 1, 5, 25, 10];
	points.sort(function(a, b){return a - b}); output: 1,5,10,25,40,100;
	points.sort(function(a, b){return b - a}); output: 100,40,25,10,5,1;

#### **_7c. Math.max.apply() & Math.min.apply on an Array_**
	Tìm min max trong array chỉ áp dụng với numberic array
##### **Syntax**
	Math.max.apply(null, arr);
##### **Example**
	var points = [40, 100, 1, 5, 25, 10];
	function myArrayMax(arr) {
  		return Math.max.apply(null, arr);
		// return Math.min.apply(null, arr);
	}
	console.log(myArrayMax(points)) //output: 100;
	// console.log(myArrayMax(points)) //output: 1;

### **_8. forEach()_**
##### **Definition and Usage**
	Executes a provided function once for each array element.
##### **Syntax**
	array.forEach(function(currentValue, index, arr), thisValue)
##### **Example**
###### **Ex1**
	const arr = [1, 2, 3, 4, 5];
		arr.forEach(item => {
 		console.log(item) // output: 1 2 3 4 5
	});
###### **Ex2**
	const numbers = [1, 2, 3, 4];
	numbers.forEach(myFunction);
	function myFunction(item, index, arr) {
		arr[index] = item * 10;
		console.log(arr[index]); // output 10 20 30 40
	}

### _**9. Map()**_
	* Creates a new array with the results of calling a provided function on every element in the calling array.
	* Tạo một array mới ánh xạ các element array cũ sang array mới
	* Không cho phép thực hiện function cho element array ko có value
	* shallow deep
##### **Syntax**
	array.map(function(currentValue, index, arr), thisValue)
##### **Example**
###### **Ex1**
	var numbers1 = [45, 4, 9, 16, 25];
	var numbers2 = numbers1.map(value => {
		console.log(value*2); // output: 90 8 18 32 25
	});

# _**#So sánh giữa forEach() & Map()**_
### _**Giống nhau**_
	Đều thực hiện hàm callback lên từng phẩn tử ở trong mảng
### _**Khác nhau:**_
#### **1. Giá trị trả về**
	* forEach() trả về undefined
	* map() trả về một mảng mới với các phần tử được chuyển đổi ngay cả khi chúng thực hiện cùng một công việc, giá trị trả về vẫn khác nhau.
##### **Example**
	const myAwesomeArray = [1, 2, 3, 4, 5]
	myAwesomeArray.forEach(x => x * x)
	//>>>>>>>>>>>>>return value: undefined

	myAwesomeArray.map(x => x * x)
	//>>>>>>>>>>>>>return value: [1, 4, 9, 16, 25]
#### **2. Khả năng xâu chuỗi các phương thức**
	* map() có thể gọi kèm theo less(), sort(), filter(), ... sau khi thực hiện phương thức map() vào một mảng.
	* không thể làm với forEach(), vì nó sẽ trả về undefined.
##### **Example**
	const myAwesomeArray = [1, 2, 3, 4, 5]
	myAwesomeArray.forEach(x => x * x).reduce((total, value) => total + value)
	//>>>>>>>>>>>>> Uncaught TypeError: Cannot read property 'reduce' of undefined

	myAwesomeArray.map(x => x * x).reduce((total, value) => total + value)
	//>>>>>>>>>>>>>return value: 55
#### **3. Khả năng biến đổi**
	* map() trả về một mảng hoàn toàn mới với các phần tử được chuyển đổi và cùng một lượng dữ liệu.
	* forEach(), ngay cả khi nó trả về undefined, nó sẽ biến đổi mảng ban đầu với hàm callback.
#### **4.Tổng kết**	
	* Những gì forEach() làm được thì map() cũng làm được
	* map() thì cấp phát bộ nhớ lưu trữ giá trị trả về, còn forEach() thì bỏ qua giá trị trả về (undefined)
	* forEach() cho phép gọi hàm callback để thay đổi mảng hiện tại, thay vào đó map() sẽ trả về một mảng mới.
	* Map() dùng khi cần dùng khi cần giá trị trả về, còn forEach() dùng nếu ko cần giá trị trả về

### _**10. filter()**_
	The filter() method creates a new array with array elements that passes a test.
##### **Syntax**
	var newArray = arr.filter(function(value,index,array),thisArg);
##### **Example**
	var numbers = [45, 4, 9, 16, 25];
	var over18 = numbers.filter(value => console.log(value > 18)); //output: true false false false true
	console.log(over18); //output: [45,18]

### _**11. reduce()**_
	* Phương thức reduce() dùng để thực thi một hàm lên từng phần tử của mảng (từ trái sang phải) với một biến tích lũy để thu về một giá trị duy nhất.
	* Phương thức reduceRight() dùng để thực thi một hàm lên từng phần tử của mảng (từ phải sang sang) với một biến tích lũy để thu về một giá trị duy nhất.
##### **Syntax**
	var reduced = arr.reduce(function(total,value,index,array),initialValue);
	// The total (the initial value / previously returned value)
##### **Example**
###### **Ex1**	
	var numbers1 = [45, 4, 9, 16, 25];
	var sum = numbers1.reduce(myFunction,10);

	function myFunction(total, value, index, array) {
		return total + value;
	}
	console.log(sum) //output: 109

###### **Ex2 (convert array to obj)**
	var arr = ["a", "b", "c"];
	var obj = arr.reduce(function(obj,value,index){
		obj[index] = value;
		return obj;
	},{});

###### **Ex3 (convert array to obj)**
	const posts = [
    	{id: 1, category: "frontend", title: "All About That Sass"},
    	{id: 2, category: "backend", title: "Beam me up, Scotty: Apache Beam tips"},
    	{id: 3, category: "frontend", title: "Sanitizing HTML: Going antibactirial on XSS attacks"}
	];

	const categoryPosts = posts.reduce((acc, posts) => {
		let {id,category } = posts;
		return {...acc, [category]: [...(acc[category] || []), id]};
	}, {});

console.log(categoryPosts);

	console.log(obj);
### _**12. indexOf**_
	The indexOf() method searches an array for an element value and returns its position.
##### **Syntax**
	arr.indexOf(element);
##### **Example**
	var fruits = ["Apple", "Orange", "Apple", "Mango"];
	console.log (fruits.indexOf("Apple")); //output: 0

### _**12. fill()**_
	* Phương thức fill() điền (sửa đổi) tất cả các phần tử của một mảng từ một chỉ mục bắt đầu (số không mặc định) 
		đến một chỉ mục kết thúc (độ dài mảng mặc định) với một giá trị tĩnh. Nó trả về mảng đã sửa đổi
	* deepcopy

##### **Syntax**
	arr.fill(new_element,position_start,position_end);
##### **Example**
	const array1 = [1, 2, 3, 4];

	// fill with 0 from position 2 until position 4
	console.log(array1.fill(0, 2, 4));
	// expected output: [1, 2, 0, 0]

	// fill with 5 from position 1
	console.log(array1.fill(5, 1));
	// expected output: [1, 5, 5, 5]

	console.log(array1.fill(6));
	// expected output: [6, 6, 6, 6]j

# **#Template literals**
### **Definition**
	Template literals are string literals allowing embedded expressions. 
	You can use multi-line strings and string interpolation features with them.
### **Example**
##### _**Ex1: Multi-line strings**_
	console.log(`string text line 1
	string text line 2`);

	output:	// "string text line 1
			// string text line 2"
##### _**Ex2: string interpolation features**_
	var a = 5;
	var b = 10;
	console.log(`Fifteen is ${a + b} and
	not ${2 * a + b}.`);

	output:	// "Fifteen is 15 and
			// not 20."
# **#Import & Export**
### **Definition**
	* ES6 cung cấp cho chúng ta import (nhập), export (xuất) các functions, biến từ module này sang module khác và sử dụng nó trong các file khác. 
	* Nói một cách chính xác theo thuật ngữ React, người ta có thể sử dụng các stateless components trong các components khác bằng cách export các components từ các modules tương ứng và sử dụng nó trong các tệp khác.

### Example
#### **Ex1**
	** Tạo các hàm được đặt tên trong một tệp JavaScript có tên là functionsFile.js **
	//functionsFile.js
	//exporting a function
	function squareNumber(x) {
  		return x * x; 
	}

	//exporting a variable 
	const pi = 3.14; 
	export {squareNumber, pi} ;

	** Tạo một tệp có tên main.js và import các giá trị được export ở trên **
	//main.js 
	import {squareNumber, pi} from "functionsFile"; 
	const radius = 7; 
	console.log("Area of a circle is", pi * squareNumber(7)); 

	//Cách khác để import

	import * as mathFuncs from "functionsFile"; 
	console.log("Area of circle is ", mathFuncs.pi * mathFuncs.squareNumber(7)); 

#### **Ex2**
	// imports
	// importing a single named export
	import { MyComponent } from "./MyComponent";

	// importing multiple named exports
	import { MyComponent, MyComponent2 } from "./MyComponent";

	// sử dụng 'as' để đặt tên khác cho named export:
	import { MyComponent2 as MyNewComponent } from "./MyComponent";

	// import tất cả named exports vào 1 đối tượng:
	import * as MainComponents from "./MyComponent";

	// exports from ./MyComponent.js file
	export const MyComponent = () => {}
	export const MyComponent2 = () => {}

# **#Function**
### _**1. Regular function**_
	Là một function thông thường có đủ name_function, giá trị trả về là một giá trị , có tham số truyền vào hoặc không
#### **Example**
	const function sum(a,b){
		console.log(a+b);
	}
	sum(1,3); //output: 4

### _**2. Anonymous function (Hàm khuyết tên)**_
	* Anonymous functions hay còn gọi là hàm ẩn danh, là một hàm được sinh ra đúng vào thời điểm chạy của chương trình.
	* Anonymous thì bạn có thể sử dụng tên biến để thay thế cho tên hàm, hoặc bạn có thể sử dụng hàm call() để invoke (thực thi).
	* Tùy vào từng trường hợp mà anonymous function rất hữu ích, 
	nếu bạn cần một function ngay tại một thời điểm thì nó rất hữu ích, hoặc bạn muốn thực hiện một callback function thì nó cũng rất hữu ích.

#### **Example**
##### **Ex1**
	const say = function(name){
		console.log('Hello ' + name);
	};
	say('Duc)'; //output: Hello Duc

##### **Ex2**
	// gọi trước hàm
	showDomain(); // Lỗi vì hàm này chưa tồn tại
 
	var showDomain = function()
	{
    	alert('Học Javascript tại Freetuts.net');
	};

	// gọi sau hàm
	showDomain(); // hoạt động vì hàm đã tồn tại

##### **Ex3**
	(function(){
    	alert('freetuts.net');
	}).call(this);
##### **Ex4**
	function caller(func)
	{
    	func();
	}
 
	caller(function(){
    	alert('Hàm callback');
	});

### _**3. Arrow function**_
##### **Definition**
	* Arrow function là một thay thế rút gọn cho hàm regular function, nhưng bị hạn chế và không thể sử dụng trong mọi trường hợp.
	* Sự khác biệt & Hạn chế:
		Không hỗ trợ binddings đến con trỏ  this hoặc super, và không nên dùng ở methods.
		Không có arguments, hoặc từ khóa new.target
		Không phù hợp với các phương thức call, apply và bind, thường dựa vào thiết lập scope.
		Không sử dụng để constructors.
		Không thể dùng yield, trong nội dung (body).

##### **Syntax**
	(param1, param2, …, paramN) => { statements }
	(param1, param2, …, paramN) => expression
	// tương đương với: (param1, param2, …, paramN) => { return expression; }

	// Dấu ngoặc đơn không bắt buộc khi chỉ có một tham số truyền vào:
	(singleParam) => { statements }
	singleParam => { statements }

	// Hàm khi không có tham số truyền vào bắt buộc phải là dấu ():
	() => { statements }
	() => expression // tương đương: () => { return expression; }

##### **Example**
	// Regular function
	function (a, b){
  		return a + b + 100;
	}

	// Arrow Function
	(a, b) => a + b + 100;

	// regular function (không đối số)
	let a = 4;
	let b = 2;
	function (){ 
  		return a + b + 100;
	}

	// Arrow function (không đối số)
	let a = 4;
	let b = 2;
	() => a + b + 100;

### _**4. Callback function**_
##### **Definition**
	Callback tức là ta truyền một đoạn code (Hàm A) này vào một đoạn code khác (Hàm B). 
	Tới một thời điểm nào đó, Hàm A sẽ được hàm B gọi lại (callback)
##### **Example**
	function greeting(name) {
  		console.log('Hello ' + name);
	}

	function processUserInput(callback) {
		var name = prompt('Please enter your name.');
		callback(name);
	}	

	processUserInput(greeting);

# **#Promise**
### **1. Definition and Usage**
	* Promise là một cơ chế trong JavaScript giúp bạn thực thi các tác vụ bất đồng bộ mà không rơi vào callback hell hay pyramid of doom, 
	là tình trạng các hàm callback lồng vào nhau ở quá nhiều tầng.
	* Một JavaScript promise object có thể là: 
		- Pending (working): Trả về undefined;
		- fullfilled: Trả về là một giá trị
		- reject: Kết quả là một error object;

### **2. Syntax**
	let myPromise = new Promise(function(myResolve, myReject){
		// "Producing Code" (May take some time)
  		myResolve(); // when successful
  		myReject();  // when error
	});

	//"Consuming code" (must wait for a fullfilled Promise)
	myPromise.then(
		function(value){
			/* code if successful */
		},
		function(error){
			/* code if some error */
		}
	);

### **3.Example**
##### **_Ex1_**
	function myDisplayer(some) {
  		document.getElementById("demo").innerHTML = some;
	}

	let myPromise = new Promise(function(myResolve, myReject) {
  		let x = 0;

	// The producing code (this may take some time)

  		if (x == 0) {
    		myResolve("OK");
  		} else {
    		myReject("Error");
  		}
		});

		myPromise.then(
  			function(value) {myDisplayer(value);},
  			function(error) {myDisplayer(error);}
	);

##### _**Ex2 (setimeout)**_
	let myPromise = new Promise(function(myResolve, myReject) {
  		setTimeout(function() { myResolve("I love You !!"); }, 3000);
		});

	myPromise.then(function(value) {
  		document.getElementById("demo").innerHTML = value;
	});

# **#Async/Await**
###	**1. Definition and Usage**
		* Async - khai báo một hàm bất đồng bộ (async function someName(){...}).
			Tự động biến đổi một hàm thông thường thành một Promise.
			Khi gọi tới hàm async nó sẽ xử lý mọi thứ và được trả về kết quả trong hàm của nó.
			Async cho phép sử dụng Await.
		* Await - tạm dừng việc thực hiện các hàm async. (Var result = await someAsyncCall ())
			Khi được đặt trước một Promise, nó sẽ đợi cho đến khi Promise kết thúc và trả về kết quả.
			Await chỉ làm việc với Promises, nó không hoạt động với callbacks.
			Await chỉ có thể được sử dụng bên trong các function async.
###	**2. Example**
##### _**Ex1**_
		async function myFunction(){
			return "Hello";
		}
		myFunction
		.then(
			value => {/*code if successfull*/}
			.catch(err => { throw err })
		.catch(err => {throw err})
		.finally(/*function*/);	
##### _**Ex2**_
	async function myDisplay() {
  		let myPromise = new Promise(function(myResolve, myReject) {
    		setTimeout(function() { 
				myResolve("I love You !!"); 
			}, 3000);
  		});
 		document.getElementById("demo").innerHTML = await myPromise;
	}

	myDisplay();
		

